package com.hangman.game.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hangman.game.model.GameCache;

@Repository("employeeRepository")
public interface GameCacheRepository extends CrudRepository<GameCache, Integer> {
	//
	GameCache findFirstByKey(String key);
}